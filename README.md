# Readme #

## Attribution ##

 - https://opengameart.org/content/a-platformer-in-the-forest
 - https://www.sfml-dev.org/
 - https://opengameart.org/content/oves-essential-game-audio-pack-collection-160-files-updated
 - https://opengameart.org/content/country-side-platform-tiles
 - https://opengameart.org/content/handpainted-platform-tileset
 - https://opengameart.org/content/explosions-0
 - https://opengameart.org/content/magic-cliffs-environment
 - https://opengameart.org/content/backgrounds-for-2d-platformers
 - https://opengameart.org/content/minimalist-pixel-tileset
 
## Description ##
 
This will be a small 2D platformer that where you fight some soldiers to take down the king.
 
## Requirements ##
 
It currently assumes SFML will be installed on your system.
 
## Screenshot ##

![ForrestRunner.png](https://bitbucket.org/repo/oLrM8yK/images/3057027862-ForrestRunner.png)
