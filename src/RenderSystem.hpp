//
// Created by stormlord on 20/02/18.
//

#ifndef TESTING_STL_RENDERSYSTEM_HPP
#define TESTING_STL_RENDERSYSTEM_HPP

#include "System.hpp"
#include "RenderComponent.hpp"
#include "Entity.hpp"

#include <SFML/Graphics.hpp>
#include <vector>

namespace Forrest
{
    class RenderSystem : virtual public System
    {
    public:
        explicit RenderSystem( std::vector< std::shared_ptr< Forrest::Entity > > &ents,
                               std::vector< std::shared_ptr< Forrest::Component > > &comps );
        void update( float delta ) override;
        void handleEvent( sf::Event evt ) override;
        void render( const std::unique_ptr< sf::RenderWindow > &window );

    private:
        RenderSystem( );
        std::vector< std::shared_ptr< Forrest::Entity > > *entities_;
        std::vector< std::shared_ptr< Forrest::Component > > *components_;

        void playWalk( RenderComponent &comp );
        void playJump( RenderComponent &comp );
        void playAttack( RenderComponent &comp );
        void playClimb( RenderComponent &comp );
        void playDie( RenderComponent &comp );
    };
}

#endif //TESTING_STL_RENDERSYSTEM_HPP
