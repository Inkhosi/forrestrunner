//
// Created by stormlord on 10/03/18.
//

#include "LevelCreator.hpp"
#include "RenderComponent.hpp"
#include "SpriteReader.hpp"

#include <iostream>

Forrest::LevelCreator::LevelCreator( std::vector< std::shared_ptr< Forrest::Entity > > *ent,
                                     std::vector< std::shared_ptr< Forrest::Component > > *comp,
                                     std::shared_ptr< sf::Texture > texBackground,
                                     std::shared_ptr< sf::Texture > texForeground,
                                     std::shared_ptr< sf::Texture > texCharacters )
{
    entities_ = ent;
    components_ = comp;
    texBackground_ = texBackground;
    texForeground_ = texForeground;
    texCharacters_ = texCharacters;
}

bool Forrest::LevelCreator::createWorld( )
{
    bool setup = setupLevel( );
    if ( !setup )
    {
        std::cout << "Failed to setup level. Exiting." << std::endl;
        return setup;
    }
    setup = setupPlayer( );
    if ( !setup )
    {
        std::cout << "Failed to setup player. Exiting." << std::endl;
        return setup;
    }
    setup = setupSoldiers( );
    if ( !setup )
    {
        std::cout << "Failed to setup soldiers. Exiting." << std::endl;
        return setup;
    }
    setup = setupKing( );
    if ( !setup )
    {
        std::cout << "Failed to setup king. Exiting." << std::endl;
        return setup;
    }
    return setup;
}

bool Forrest::LevelCreator::setupLevel( )
{
    auto rcBackground = std::make_shared< RenderComponent >( sf::IntRect( 0, 0, 1920, 1080 ),
                                                             sf::Vector2f( .75f, .75f ),
                                                             sf::Vector2f( 0.0f, 0.0f ),
                                                             sf::Vector2f( 0.0f, 0.0f ),
                                                             texBackground_, false );
    components_->emplace_back( rcBackground );
    auto background = std::make_shared< Forrest::Entity >( );
    background->add_component( rcBackground->getId( ), ComponentType::RenderComp );
    entities_->emplace_back( background );
    return true;
}

bool Forrest::LevelCreator::setupPlayer( )
{
    // Create the render component
    auto rcPlayer = std::make_shared< RenderComponent >( sf::IntRect( 0, 0, 32, 32 ),
                                                         sf::Vector2f( 1.f, 1.f ),
                                                         sf::Vector2f( 16, 32 ),
                                                         sf::Vector2f( 400, 600 ),
                                                         texCharacters_, true );
    // Setup animations
    Forrest::SpriteReader sr;
    sr.readFile( "../sprites/player_sprites.txt" );
    rcPlayer->animAttack = sr.getAttackAnim( );
    rcPlayer->animClimb = sr.getClimbAnim( );
    rcPlayer->animDie = sr.getDieAnim( );
    rcPlayer->animJump = sr.getJumpAnim( );
    rcPlayer->animWalk = sr.getWalkAnim( );
    // Store the component
    components_->emplace_back( rcPlayer );
    // Create the entity
    auto player = std::make_shared< Forrest::Entity >( );
    // Add the components the make the attributes
    player->add_component( rcPlayer->getId( ), ComponentType::RenderComp );
    // Store the entity
    entities_->emplace_back( player );
    return true;
}

bool Forrest::LevelCreator::setupSoldiers( )
{
    Forrest::SpriteReader sr;
    sr.readFile( "../sprites/soldier_sprites.txt" );
    auto rcSoldier1 = std::make_shared< RenderComponent >( sf::IntRect( 0, 64, 32, 32 ),
                                                           sf::Vector2f( 1.f, 1.f ),
                                                           sf::Vector2f( 16, 32 ),
                                                           sf::Vector2f( 300, 600 ),
                                                           texCharacters_, true );
    rcSoldier1->animAttack = sr.getAttackAnim( );
    rcSoldier1->animClimb = sr.getClimbAnim( );
    rcSoldier1->animDie = sr.getDieAnim( );
    rcSoldier1->animJump = sr.getJumpAnim( );
    rcSoldier1->animWalk = sr.getWalkAnim( );
    components_->emplace_back( rcSoldier1 );
    auto rcSoldier2 = std::make_shared< RenderComponent >( sf::IntRect( 0, 64, 32, 32 ),
                                                           sf::Vector2f( -1.f, 1.f ),
                                                           sf::Vector2f( 16, 32 ),
                                                           sf::Vector2f( 500, 600 ),
                                                           texCharacters_, true );
    rcSoldier2->animAttack = sr.getAttackAnim( );
    rcSoldier2->animClimb = sr.getClimbAnim( );
    rcSoldier2->animDie = sr.getDieAnim( );
    rcSoldier2->animJump = sr.getJumpAnim( );
    rcSoldier2->animWalk = sr.getWalkAnim( );
    components_->emplace_back( rcSoldier2 );
    auto soldier1 = std::make_shared< Forrest::Entity >( );
    soldier1->add_component( rcSoldier1->getId( ), ComponentType::RenderComp );
    auto soldier2 = std::make_shared< Forrest::Entity >( );
    soldier2->add_component( rcSoldier2->getId( ), ComponentType::RenderComp );
    entities_->emplace_back( soldier1 );
    entities_->emplace_back( soldier2 );
    return true;
}

bool Forrest::LevelCreator::setupKing( )
{
    Forrest::SpriteReader sr;
    sr.readFile( "../sprites/king_sprites.txt" );
    auto rcKing = std::make_shared< RenderComponent >( sf::IntRect( 0, 32, 32, 32 ),
                                                       sf::Vector2f( -1.f, 1.f ),
                                                       sf::Vector2f( 16, 32 ),
                                                       sf::Vector2f( 700, 600 ),
                                                       texCharacters_, true );
    rcKing->animAttack = sr.getAttackAnim( );
    rcKing->animClimb = sr.getClimbAnim( );
    rcKing->animDie = sr.getDieAnim( );
    rcKing->animJump = sr.getJumpAnim( );
    rcKing->animWalk = sr.getWalkAnim( );
    components_->emplace_back( rcKing );
    auto king = std::make_shared< Forrest::Entity >( );
    king->add_component( rcKing->getId( ), ComponentType::RenderComp );
    entities_->emplace_back( king );
    return true;
}
