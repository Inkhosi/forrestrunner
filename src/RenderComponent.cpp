//
// Created by stormlord on 20/02/18.
//

#include "RenderComponent.hpp"

Forrest::RenderComponent::RenderComponent( sf::IntRect rect, sf::Vector2f scale, sf::Vector2f origin,
                                           sf::Vector2f position, std::shared_ptr< sf::Texture > tex,
                                           bool anims = false )
{
    sprite = std::make_unique< sf::Sprite >( );
    sprite->setTexture( *tex );
    sprite->setTextureRect( rect );
    sprite->setScale( scale );
    sprite->setOrigin( origin );
    sprite->setPosition( position );
    animated = anims;
    frame = 0;
    currentAnimation = Animation::Nothing;
    defaultRect = rect;
    animWalk = { };
    animAttack = { };
    animClimb = { };
    animJump = { };
    animDie = { };
}
