//
// Created by stormlord on 20/02/18.
//

#include "Game.hpp"
#include "LevelCreator.hpp"

#include <SFML/Graphics.hpp>
#include <memory>
#include <iostream>
#include <chrono>

Forrest::Game::Game( )
{
    // General
    width_ = 800;
    height_ = 600;
    // E & C
    entities_ = { };
    components_ = { };
    // Systems
    renderSystem_ = nullptr;
    physicsSystem_ = nullptr;
    // Textures
    texBackground_ = nullptr;
    texForeground_ = nullptr;
    texCharacter_ = nullptr;
    // Window
    window_ = nullptr;
}

bool Forrest::Game::setup( )
{
    // Load Resources
    loadResources( );
    // Create the game subsystems
    createSubsystems( );
    // Create event handler
    eventHandler_ = std::make_unique< Forrest::EventHandler >( renderSystem_, physicsSystem_ );
    // Create window
    window_ = std::make_unique< sf::RenderWindow >( sf::VideoMode( width_, height_ ), "Forrest Runner" );
    window_->setVerticalSyncEnabled( true );
    // Play God
    createWorld( );
    return true;
}

bool Forrest::Game::loadResources( )
{
    texBackground_ = std::make_shared< sf::Texture >( );
    texBackground_->loadFromFile( "../images/rock.png" );
    texForeground_ = std::make_shared< sf::Texture >( );
    texForeground_->loadFromFile( "../images/sheet.png" );
    texCharacter_ = std::make_shared< sf::Texture >( );
    texCharacter_->loadFromFile( "../images/characters.png" );
    return true;
}

bool Forrest::Game::createSubsystems( )
{
    renderSystem_ = std::make_shared< Forrest::RenderSystem >( entities_, components_ );
    physicsSystem_ = std::make_shared< Forrest::PhysicsSystem >( entities_, components_ );
    // TODO audio
    return true;
}

bool Forrest::Game::createWorld( )
{
    auto lc = std::make_unique< LevelCreator >( &entities_, &components_, texBackground_, texForeground_,
                                                texCharacter_ );
    return lc->createWorld( );
}

void Forrest::Game::run( )
{
    std::chrono::high_resolution_clock::time_point last, current;
    std::chrono::duration< float > deltaT = { };
    while ( window_->isOpen( ) )
    {
        current = std::chrono::high_resolution_clock::now( );
        deltaT = std::chrono::duration_cast< std::chrono::duration< float > >( current - last );
        // Handle events
        eventHandler_->handleEvents( window_ );
        // Update the world
        if ( deltaT.count( ) > .1f )
        {
            // TODO this update needs to happen on a fixed deltaT
            physicsSystem_->update( deltaT.count( ) );
            renderSystem_->update( deltaT.count( ) );
            last = current;
        }
        // Render the world
        renderSystem_->render( window_ );
    }
}
