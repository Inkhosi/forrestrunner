//
// Created by stormlord on 05/03/18.
//

#ifndef TESTING_STL_SYSTEM_HPP
#define TESTING_STL_SYSTEM_HPP

#include <SFML/Graphics.hpp>

class System
{
public:
    virtual void handleEvent( sf::Event event ) = 0;
    virtual void update( float delta ) = 0; // This makes more sense
};

#endif //TESTING_STL_SYSTEM_HPP
