//
// Created by stormlord on 20/02/18.
//

#ifndef TESTING_STL_ENTITY_HPP
#define TESTING_STL_ENTITY_HPP

#include "Component.hpp"

#include <string>
#include <vector>
#include <map>
#include <SFML/Window/Event.hpp>

namespace Forrest
{
    class Entity
    {
    public:
        Entity( );
        bool add_component( unsigned int id, ComponentType type );
        bool remove_component( ComponentType type );
        unsigned int find_component( ComponentType type ); // This needs some thought
        unsigned int getId( ) const;
        void handleEvent( sf::Event &evt );

    private:
        std::string name_;
        unsigned int id_;
        static unsigned int idGen_;
        std::map< ComponentType, unsigned int > components_;
    };
}

#endif //TESTING_STL_ENTITY_HPP
