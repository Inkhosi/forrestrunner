//
// Created by stormlord on 03/03/18.
//

#include "PhysicsComponent.hpp"

PhysicsComponent::PhysicsComponent( sf::Vector2f pos, sf::Vector2i bbox, sf::Vector2f orig )
{
    position_ = pos;
    box_ = bbox;
    origin_ = orig;
}

void PhysicsComponent::setPosition( sf::Vector2f pos )
{
    position_ = pos;
}

void PhysicsComponent::setBoundingBox( sf::Vector2i bbox )
{
    box_ = bbox;
}

void PhysicsComponent::setOrigin( sf::Vector2f orig )
{
    origin_ = orig;
}

sf::Vector2f PhysicsComponent::getPosition( ) const
{
    return position_;
}

sf::Vector2i PhysicsComponent::getBoundingBox( ) const
{
    return box_;
}

sf::Vector2f PhysicsComponent::getOrigin( ) const
{
    return origin_;
}
