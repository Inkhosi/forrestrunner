//
// Created by stormlord on 03/03/18.
//

#ifndef TESTING_STL_PHYSICSCOMPONENT_HPP
#define TESTING_STL_PHYSICSCOMPONENT_HPP


#include <SFML/System.hpp>

class PhysicsComponent
{
public:
    explicit PhysicsComponent( sf::Vector2f pos, sf::Vector2i bbox, sf::Vector2f orig );
    void setPosition( sf::Vector2f pos );
    void setBoundingBox( sf::Vector2i bbox );
    void setOrigin( sf::Vector2f orig );
    sf::Vector2f getPosition( ) const;
    sf::Vector2i getBoundingBox( ) const;
    sf::Vector2f getOrigin( ) const;

private:
    sf::Vector2f position_;
    sf::Vector2i box_;
    sf::Vector2f origin_;
};


#endif //TESTING_STL_PHYSICSCOMPONENT_HPP
