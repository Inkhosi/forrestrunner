//
// Created by stormlord on 20/02/18.
//

#include "Component.hpp"

// Initialize the ID generator for Component
unsigned int Forrest::Component::idGen_ = 1;

Forrest::Component::Component( )
{
    id_ = idGen_++; // A new ID for each Entity
}

unsigned int Forrest::Component::getId( ) const
{
    return id_;
}
