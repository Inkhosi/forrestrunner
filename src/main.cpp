//
// Created by stormlord on 06/02/18.
//

#include "Game.hpp"

#include <memory>

//
int main( )
{
    auto game = std::make_unique< Forrest::Game >( );
    if ( game->setup( ) )
    {
        game->run( );
    }
    return 0;
}
