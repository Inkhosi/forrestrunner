//
// Created by stormlord on 10/03/18.
//

#ifndef TESTING_STL_LEVELCREATOR_HPP
#define TESTING_STL_LEVELCREATOR_HPP

#include "Entity.hpp"

#include <vector>
#include <memory>
#include <SFML/Graphics/Texture.hpp>

namespace Forrest
{
    class LevelCreator
    {
    public:
        LevelCreator( std::vector< std::shared_ptr< Forrest::Entity > > *ent,
                      std::vector< std::shared_ptr< Forrest::Component > > *comp,
                      std::shared_ptr< sf::Texture > texBackground,
                      std::shared_ptr< sf::Texture > texForeground,
                      std::shared_ptr< sf::Texture > texCharacters );
        bool createWorld( );
    private:
        std::vector< std::shared_ptr< Forrest::Entity > > *entities_;
        std::vector< std::shared_ptr< Forrest::Component > > *components_;
        std::shared_ptr< sf::Texture > texBackground_;
        std::shared_ptr< sf::Texture > texForeground_;
        std::shared_ptr< sf::Texture > texCharacters_;

        bool setupLevel( );
        bool setupPlayer( );
        bool setupSoldiers( );
        bool setupKing( );
    };
}

#endif //TESTING_STL_LEVELCREATOR_HPP
