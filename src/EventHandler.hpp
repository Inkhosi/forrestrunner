//
// Created by stormlord on 28/02/18.
//

#ifndef TESTING_STL_EVENTHANDLER_HPP
#define TESTING_STL_EVENTHANDLER_HPP

#include "Entity.hpp"
#include "RenderSystem.hpp"
#include "PhysicsSystem.hpp"

#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>

namespace Forrest
{
    class EventHandler
    {
    public:
        EventHandler( std::shared_ptr< Forrest::RenderSystem > renderSys,
                      std::shared_ptr< Forrest::PhysicsSystem > physicsSys );
        void handleEvents( std::unique_ptr< sf::RenderWindow > &window );

    private:
        EventHandler( );
        std::shared_ptr< Forrest::RenderSystem > renderSystem_;
        std::shared_ptr< Forrest::PhysicsSystem > physicsSystem_;
        sf::Event event_;
    };
}

#endif //TESTING_STL_EVENTHANDLER_HPP
