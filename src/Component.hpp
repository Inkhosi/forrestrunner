//
// Created by stormlord on 20/02/18.
//

#ifndef TESTING_STL_COMPONENT_HPP
#define TESTING_STL_COMPONENT_HPP

namespace Forrest
{
    enum ComponentType
    {
        RenderComp = 0,
        PhysicsComp,
        AudioComp,
        EventComp,
    };

    class Component
    {
    public:
        Component( );
        unsigned int getId( ) const;

        virtual ~Component( ) = default;

    private:
        unsigned int id_;
        static unsigned int idGen_;
    };
}

#endif //TESTING_STL_COMPONENT_HPP
