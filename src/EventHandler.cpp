//
// Created by stormlord on 28/02/18.
//

#include "EventHandler.hpp"

Forrest::EventHandler::EventHandler( std::shared_ptr< Forrest::RenderSystem > renderSys,
                                     std::shared_ptr< Forrest::PhysicsSystem > physicsSys )
{
    renderSystem_ = renderSys;
    physicsSystem_ = physicsSys;
}

void Forrest::EventHandler::handleEvents( std::unique_ptr< sf::RenderWindow > &window )
{
    while ( window->pollEvent( event_ ) )
    {
        if ( event_.type == sf::Event::Closed )
        {
            window->close( );
        }
        if ( event_.type == sf::Event::KeyPressed )
        {
            if ( event_.key.code == sf::Keyboard::Escape )
            {
                window->close( );
            }
            physicsSystem_->handleEvent( event_ );
            renderSystem_->handleEvent( event_ );
        }
        if ( event_.type == sf::Event::KeyReleased )
        {
            physicsSystem_->handleEvent( event_ );
            renderSystem_->handleEvent( event_ );
        }
    }
}

Forrest::EventHandler::EventHandler( )
{
    renderSystem_ = nullptr;
    physicsSystem_ = nullptr;
}
