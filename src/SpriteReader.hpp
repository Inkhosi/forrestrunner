//
// Created by stormlord on 11/03/18.
//

#ifndef TESTING_STL_SPRITEREADER_HPP
#define TESTING_STL_SPRITEREADER_HPP


#include <SFML/Graphics/Rect.hpp>
#include <vector>
#include <string>

namespace Forrest
{
    class SpriteReader
    {
    public:
        bool readFile( std::string file );
        std::vector< sf::IntRect > getAttackAnim( ) const;
        std::vector< sf::IntRect > getClimbAnim( ) const;
        std::vector< sf::IntRect > getDieAnim( ) const;
        std::vector< sf::IntRect > getJumpAnim( ) const;
        std::vector< sf::IntRect > getWalkAnim( ) const;

    private:
        std::vector< sf::IntRect > attack_ = { };
        std::vector< sf::IntRect > climb_ = { };
        std::vector< sf::IntRect > die_ = { };
        std::vector< sf::IntRect > jump_ = { };
        std::vector< sf::IntRect > walk_ = { };
    };
}

#endif //TESTING_STL_SPRITEREADER_HPP
