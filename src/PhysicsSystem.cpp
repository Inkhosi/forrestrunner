//
// Created by stormlord on 03/03/18.
//

#include "PhysicsSystem.hpp"

Forrest::PhysicsSystem::PhysicsSystem( std::vector< std::shared_ptr< Forrest::Entity > > &ents,
                                       std::vector< std::shared_ptr< Forrest::Component > > &comps )
{
    entities_ = &ents;
    components_ = &comps;
}

void Forrest::PhysicsSystem::handleEvent( sf::Event evt )
{
    // TODO handle input
}

void Forrest::PhysicsSystem::update( float delta )
{
    // TODO update world by time delta
}

Forrest::PhysicsSystem::PhysicsSystem( )
{
    entities_ = nullptr;
    components_ = nullptr;
}
