//
// Created by stormlord on 11/03/18.
//

#include "SpriteReader.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <list>

bool Forrest::SpriteReader::readFile( std::string file )
{
    std::list< std::pair< std::string, std::vector< sf::IntRect > * > > stores = {
            std::make_pair( "[attack]", &attack_ ),
            std::make_pair( "[climb]", &climb_ ),
            std::make_pair( "[die]", &die_ ),
            std::make_pair( "[jump]", &jump_ ),
            std::make_pair( "[walk]", &walk_ )
    };
    std::string line;
    std::ifstream input( file );
    std::vector< sf::IntRect > *store = nullptr;
    if ( input.is_open( ) )
    {
        while ( std::getline( input, line ) )
        {
            // Check if line needs processing
            if ( line.empty( ) || line.at( 0 ) == '#' )
            {
                continue;
            }
            bool foundSection = false;
            for ( auto it : stores )
            {
                if ( line.find( it.first ) != std::string::npos )
                {
                    store = it.second;
                    foundSection = true;
                    break;
                }
            }
            if ( foundSection )
            {
                continue; // Skip section header
            }
            // Read the sf::IntRect values
            std::stringstream ss( line );
            unsigned int x = 0, y = 0, w = 0, h = 0;
            ss >> x >> y >> w >> h;
            store->push_back( sf::IntRect( x, y, w, h ) );
        }
        input.close( );
        return true;
    }
    return false;
}

std::vector< sf::IntRect > Forrest::SpriteReader::getAttackAnim( ) const
{
    return attack_;
}

std::vector< sf::IntRect > Forrest::SpriteReader::getClimbAnim( ) const
{
    return climb_;
}

std::vector< sf::IntRect > Forrest::SpriteReader::getDieAnim( ) const
{
    return die_;
}

std::vector< sf::IntRect > Forrest::SpriteReader::getJumpAnim( ) const
{
    return jump_;
}

std::vector< sf::IntRect > Forrest::SpriteReader::getWalkAnim( ) const
{
    return walk_;
}
