//
// Created by stormlord on 20/02/18.
//

#ifndef TESTING_STL_GAME_HPP
#define TESTING_STL_GAME_HPP

#include "Entity.hpp"
#include "RenderComponent.hpp"
#include "RenderSystem.hpp"
#include "EventHandler.hpp"
#include "PhysicsSystem.hpp"

#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>

namespace Forrest
{
    class Game
    {
    public:
        Game( );
        bool setup( );
        void run( );

    private:
        bool loadResources( );
        bool createSubsystems( );
        bool createWorld( );

        std::vector< std::shared_ptr< Forrest::Entity > > entities_;
        std::vector< std::shared_ptr< Forrest::Component > > components_;

        std::shared_ptr< Forrest::RenderSystem > renderSystem_;
        std::shared_ptr< Forrest::PhysicsSystem > physicsSystem_;
        // TODO audio system

        std::unique_ptr< Forrest::EventHandler > eventHandler_;
        std::unique_ptr< sf::RenderWindow > window_;

        std::shared_ptr< sf::Texture > texBackground_;
        std::shared_ptr< sf::Texture > texForeground_;
        std::shared_ptr< sf::Texture > texCharacter_;

        unsigned int width_ = 800, height_ = 600;
    };
}

#endif //TESTING_STL_GAME_HPP
