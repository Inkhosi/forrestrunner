//
// Created by stormlord on 20/02/18.
//

#ifndef TESTING_STL_RENDERCOMPONENT_HPP
#define TESTING_STL_RENDERCOMPONENT_HPP

#include "Component.hpp"

#include <SFML/Graphics.hpp>
#include <memory>

namespace Forrest
{
    enum Animation
    {
        Nothing = -1,
        Attack = 0,
        Climb,
        Die,
        Jump,
        Walk
    };

    class RenderComponent : virtual public Component
    {
    public:
        RenderComponent( sf::IntRect rect, sf::Vector2f scale, sf::Vector2f origin, sf::Vector2f position,
                         std::shared_ptr< sf::Texture > tex, bool anims );
        std::unique_ptr< sf::Sprite > sprite;
        bool animated;
        unsigned long frame, maxFrame;
        Animation currentAnimation;
        sf::IntRect defaultRect;
        std::vector< sf::IntRect > animWalk;
        std::vector< sf::IntRect > animJump;
        std::vector< sf::IntRect > animClimb;
        std::vector< sf::IntRect > animAttack;
        std::vector< sf::IntRect > animDie;
    };
}

#endif //TESTING_STL_RENDERCOMPONENT_HPP
