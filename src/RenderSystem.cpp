//
// Created by stormlord on 20/02/18.
//

#include "RenderSystem.hpp"

#include <iostream>

Forrest::RenderSystem::RenderSystem( )
{
    entities_ = nullptr;
    components_ = nullptr;
}

Forrest::RenderSystem::RenderSystem( std::vector< std::shared_ptr< Forrest::Entity > > &ents,
                                     std::vector< std::shared_ptr< Forrest::Component > > &comps )
{
    entities_ = &ents;
    components_ = &comps;
}

void Forrest::RenderSystem::handleEvent( sf::Event evt )
{
    // Update the entities we care about
    auto compId = entities_->at( 1 )->find_component( ComponentType::RenderComp );
    std::shared_ptr< RenderComponent > comp = nullptr;
    for ( const auto &it: *components_ )
    {
        if ( it->getId( ) == compId )
        {
            comp = std::dynamic_pointer_cast< RenderComponent >( it );
            break;
        }
    }
    if ( evt.type == sf::Event::KeyPressed )
    {
        if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Space ) )
        {
            playJump( *comp );
        }
        if ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )
        {
            if ( comp->currentAnimation != Animation::Walk )
            {
                playWalk( *comp );
                if ( comp->sprite->getScale( ).x > 0 )
                {
                    comp->sprite->scale( sf::Vector2f( -1.f, 1.f ) ); // Face left
                }
            }
        }
        if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )
        {
            if ( comp->currentAnimation != Animation::Walk )
            {
                playWalk( *comp );
                if ( comp->sprite->getScale( ).x < 0 )
                {
                    comp->sprite->scale( sf::Vector2f( -1.f, 1.f ) ); // Face right
                }
            }
        }
        if ( sf::Keyboard::isKeyPressed( sf::Keyboard::F ) )
        {
            if ( comp->currentAnimation != Animation::Attack )
            {
                playAttack( *comp );
            }
        }
    }
    if ( evt.type == sf::Event::KeyReleased )
    {
        if ( evt.key.code == sf::Keyboard::A )
        {
            comp->currentAnimation = Animation::Nothing;
        }
        if ( evt.key.code == sf::Keyboard::D )
        {
            comp->currentAnimation = Animation::Nothing;
        }
    }
}

void Forrest::RenderSystem::update( float delta )
{
    for ( const auto &ent : *entities_ )
    {
        unsigned int id = ent->find_component( ComponentType::RenderComp );
        for ( const auto &it: *components_ )
        {
            if ( it->getId( ) == id )
            {
                auto comp = std::dynamic_pointer_cast< RenderComponent >( it );
                if ( comp == nullptr )
                {
                    std::cout << "Cast failed." << std::endl;
                    break;
                }
                if ( !comp->animated )
                {
                    break; // Nothing to animate
                }
                if ( comp->currentAnimation != Animation::Nothing )
                {
                    if ( comp->frame == comp->maxFrame )
                    {
                        comp->frame = 0;
                        if ( comp->currentAnimation == Animation::Jump ||
                             comp->currentAnimation == Animation::Attack )
                        {
                            comp->currentAnimation = Animation::Nothing;
                        }
                    }
                    switch ( comp->currentAnimation )
                    {
                        case Animation::Attack:
                            comp->sprite->setTextureRect( comp->animAttack.at( comp->frame ) );
                            break;
                        case Animation::Climb:
                            comp->sprite->setTextureRect( comp->animClimb.at( comp->frame ) );
                            break;
                        case Animation::Die:
                            comp->sprite->setTextureRect( comp->animDie.at( comp->frame ) );
                            break;
                        case Animation::Jump:
                            comp->sprite->setTextureRect( comp->animJump.at( comp->frame ) );
                            break;
                        case Animation::Walk:
                            comp->sprite->setTextureRect( comp->animWalk.at( comp->frame ) );
                            break;
                        default:
                            comp->sprite->setTextureRect( comp->defaultRect );
                            break;
                    }
                    comp->frame++;
                }
                else
                {
                    comp->sprite->setTextureRect( comp->defaultRect );
                }
            }
        }
    }
}

void Forrest::RenderSystem::render( const std::unique_ptr< sf::RenderWindow > &window )
{
    window->clear( sf::Color( 122, 122, 122 ) );
    // Draw all entities
    for ( const auto &ent : *entities_ )
    {
        unsigned int id = ent->find_component( ComponentType::RenderComp );
        for ( const auto &it: *components_ )
        {
            if ( it->getId( ) == id )
            {
                auto comp = std::dynamic_pointer_cast< RenderComponent >( it );
                if ( comp == nullptr )
                {
                    std::cout << "Cast failed." << std::endl;
                    break;
                }
                else
                {
                    window->draw( *( comp->sprite ) );
                }
                break; // Stop looking
            }
        }
    }
    window->display( );
}

void Forrest::RenderSystem::playWalk( Forrest::RenderComponent &comp )
{
    comp.currentAnimation = Animation::Walk;
    comp.frame = 0;
    if ( comp.animWalk.empty( ) )
    {
        comp.maxFrame = 0;
    }
    else
    {
        comp.maxFrame = comp.animWalk.size( ) - 1;
    }
}

void Forrest::RenderSystem::playJump( Forrest::RenderComponent &comp )
{
    comp.currentAnimation = Animation::Jump;
    comp.frame = 0;
    if ( comp.animJump.empty( ) )
    {
        comp.maxFrame = 0;
    }
    else
    {
        comp.maxFrame = comp.animJump.size( ) - 1;
    }
}

void Forrest::RenderSystem::playAttack( Forrest::RenderComponent &comp )
{
    comp.currentAnimation = Animation::Attack;
    comp.frame = 0;
    if ( comp.animAttack.empty( ) )
    {
        comp.maxFrame = 0;
    }
    else
    {
        comp.maxFrame = comp.animAttack.size( ) - 1;
    }
}

void Forrest::RenderSystem::playClimb( Forrest::RenderComponent &comp )
{
    comp.currentAnimation = Animation::Climb;
    comp.frame = 0;
    if ( comp.animClimb.empty( ) )
    {
        comp.maxFrame = 0;
    }
    else
    {
        comp.maxFrame = comp.animClimb.size( ) - 1;
    }
}

void Forrest::RenderSystem::playDie( Forrest::RenderComponent &comp )
{
    comp.currentAnimation = Animation::Die;
    comp.frame = 0;
    if ( comp.animDie.empty( ) )
    {
        comp.maxFrame = 0;
    }
    else
    {
        comp.maxFrame = comp.animDie.size( ) - 1;
    }
}
