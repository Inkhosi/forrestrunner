//
// Created by stormlord on 20/02/18.
//

#include "Entity.hpp"

#include <algorithm>
#include <utility>

// Initialize the ID generator for Entity
unsigned int Forrest::Entity::idGen_ = 1;

Forrest::Entity::Entity( )
{
    id_ = idGen_++; // A new id for each Entity
    components_ = std::map< ComponentType, unsigned int >( );
}

bool Forrest::Entity::add_component( unsigned int id, Forrest::ComponentType type )
{
    components_.insert( std::pair< Forrest::ComponentType, unsigned int >( type, id ) );
    return true;
}

bool Forrest::Entity::remove_component( ComponentType type )
{
    components_.erase( components_.find( type ) );
    return true;
}

unsigned int Forrest::Entity::getId( ) const
{
    return id_;
}

unsigned int Forrest::Entity::find_component( Forrest::ComponentType type )
{
    auto comp = components_.find( type );
    return comp->second;
}

void Forrest::Entity::handleEvent( sf::Event &evt )
{
    if ( evt.type == sf::Event::KeyPressed )
    {
        // TODO so how do I talk to the components?
        components_[ Forrest::ComponentType::RenderComp ];
        components_[ Forrest::ComponentType::PhysicsComp ];
    }
    if ( evt.type == sf::Event::KeyReleased )
    {
        // TODO same problem
        components_[ Forrest::ComponentType::RenderComp ];
        components_[ Forrest::ComponentType::PhysicsComp ];
    }
}
