//
// Created by stormlord on 03/03/18.
//

#ifndef TESTING_STL_PHYSICSSYSTEM_HPP
#define TESTING_STL_PHYSICSSYSTEM_HPP

#include "System.hpp"
#include "Component.hpp"
#include "Entity.hpp"

#include <SFML/Graphics.hpp>
#include <memory>

namespace Forrest
{
    class PhysicsSystem : virtual public System
    {
    public:
        PhysicsSystem( std::vector< std::shared_ptr< Forrest::Entity > > &ents,
                       std::vector< std::shared_ptr< Forrest::Component > > &comps );
        void handleEvent( sf::Event evt ) override;
        void update( float delta ) override;

    private:
        PhysicsSystem( );
        std::vector< std::shared_ptr< Forrest::Entity > > *entities_;
        std::vector< std::shared_ptr< Forrest::Component > > *components_;
    };
}

#endif //TESTING_STL_PHYSICSSYSTEM_HPP
